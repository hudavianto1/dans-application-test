package com.dans.multipro.test.constants;

/**
 * @author hudavianto
 */
public class UserFields {

    public static final String tableNameUser = "m_user";


    public static final String userId = "user_id";
    public static final String userName = "username";
    public static final String password = "password";
    public static final String name = "name";
}
